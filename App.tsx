/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useState} from 'react';
import {StatusBar} from 'react-native';
import UnitInput from './components/UnitInput';
import styled from '@emotion/native';
import useMeasurements from './hooks/useMeasurements';
import RNPickerSelect from 'react-native-picker-select';

const SafeContentView = styled.SafeAreaView({
  flex: 1,
});

const ContentContainer = styled.View({
  alignItems: 'center',
  justifyContent: 'center',
  flex: 1,
  padding: 12,
});

const SaveButton = styled.TouchableOpacity({
  height: 60,
  alignSelf: 'stretch',
  borderRadius: 16,
  backgroundColor: 'green',
  margin: 16,
  alignItems: 'center',
  justifyContent: 'center',
});

const SaveButtonLabel = styled.Text({
  fontSize: 20,
  color: 'white',
});

const units = [
  {label: 'Imperial', value: 'imperial'},
  {label: 'Metric', value: 'metric'},
];

const App = () => {
  const {
    pounds,
    feet,
    inches,
    kilos,
    meters,
    unit,
    convertAndSetUnit,
    setPounds,
    setFeet,
    setInches,
    setKilos,
    setMeters,
    saveToDisk,
  } = useMeasurements();

  const [fileSaved, setFileSaved] = useState(false);

  const saveFile = () => {
    saveToDisk()
      .then(() => {
        setFileSaved(true);
        setTimeout(() => setFileSaved(false), 1500);
      })
      .catch((error: Error) => console.log(error.message));
  };

  return (
    <SafeContentView>
      <StatusBar />
      <ContentContainer>
        {unit === 'imperial' ? (
          <>
            <UnitInput
              label="lbs"
              value={pounds.toString()}
              onChangeText={(input: string) => setPounds(Number(input))}
            />
            <UnitInput
              label="ft"
              value={feet.toString()}
              onChangeText={(input: string) => setFeet(Number(input))}
            />
            <UnitInput
              label="in"
              value={inches.toString()}
              onChangeText={(input: string) => setInches(Number(input))}
            />
          </>
        ) : (
          <>
            <UnitInput
              label="kg"
              value={kilos.toString()}
              onChangeText={(input: string) => setKilos(Number(input))}
            />
            <UnitInput
              label="m"
              value={meters.toString()}
              onChangeText={(input: string) => setMeters(Number(input))}
            />
          </>
        )}
        <RNPickerSelect
          touchableWrapperProps={{
            testID: 'unit-picker',
            style: {
              height: 40,
              justifyContent: 'center',
              borderRadius: 2,
              padding: 8,
              margin: 12,
              backgroundColor: 'lightgrey',
            },
          }}
          onValueChange={value => convertAndSetUnit(value)}
          items={units}
          value={unit}
        />
        <SaveButton onPress={saveFile}>
          <SaveButtonLabel>{fileSaved ? 'Saved!' : 'Save'}</SaveButtonLabel>
        </SaveButton>
      </ContentContainer>
    </SafeContentView>
  );
};

export default App;
