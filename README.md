Prerequisites:

Ensure you have your Android and/or iOS dev environments set up, following the instructions here: https://reactnative.dev/docs/environment-setup


To run on Android:

- Run yarn (or npm install) in root directory
- Run yarn android (or npm run android) in root directory

To run on iOS:

- Run yarn (or npm install) in root directory
- Navigate to ios directory
- Run pod install
- Navigate back to root directory
- Run yarn ios (or npm run ios) in root directory
