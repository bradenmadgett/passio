/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
import {fireEvent, render, waitFor} from '@testing-library/react-native';

const mockReadFile = jest.fn(() => Promise.resolve('{}'));

jest.mock('react-native-fs', () => ({
  DocumentDirectoryPath: '',
  readFile: () => mockReadFile(),
  writeFile: jest.fn(),
}));

test('renders correctly', async () => {
  const {getByText} = render(<App />);
  await waitFor(() => expect(getByText('Save')).toBeTruthy());
});

test('renders with default values', async () => {
  const {getAllByDisplayValue} = render(<App />);

  // This test will check for input fields with value of 0 (pounds, feet, inches)
  await waitFor(() => expect(getAllByDisplayValue('0').length).toBe(3));
});

test('Values are converted when unit is changed', async () => {
  mockReadFile.mockImplementationOnce(() =>
    Promise.resolve(
      JSON.stringify({
        pounds: 5,
        feet: 5,
        inches: 6,
      }),
    ),
  );
  const {getByText, getByTestId, getAllByDisplayValue, getByDisplayValue} = render(<App />);

  const lbs = await waitFor(() => getByText('lbs'));
  const ft = await waitFor(() => getByText('ft'));
  const inch = await waitFor(() => getByText('in'));
  expect(lbs).toBeTruthy();
  expect(ft).toBeTruthy();
  expect(inch).toBeTruthy();

  //Should initialize with read file data
  const originalInputs = await waitFor(() => getAllByDisplayValue('5'));
  expect(originalInputs.length).toBe(2);

  fireEvent(getByTestId('unit-picker'), 'onValueChange', 'metric');

  const meter = await waitFor(() => getByText('m'));
  const kilo = await waitFor(() => getByText('kg'));
  expect(meter).toBeTruthy();
  expect(kilo).toBeTruthy();

  //Should show converted units
  const convertedKilo = await waitFor(() => getByDisplayValue('2.26796'));
  const convertedMeter = await waitFor(() => getByDisplayValue('1.6764'));
  expect(convertedKilo).toBeTruthy();
  expect(convertedMeter).toBeTruthy();
});
