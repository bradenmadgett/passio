import React from 'react';
import styled from '@emotion/native';
import {TextInputProps} from 'react-native';

interface UnitInputProps {
  label?: string;
  flex?: number;
}

const UnitRow = styled.View<{flex?: number}>(props => ({
  flexDirection: 'row',
  alignItems: 'center',
  flex: props.flex,
}));

const UnitNumberInput = styled.TextInput({
  backgroundColor: 'lightgrey',
  flex: 1,
  borderRadius: 2,
  margin: 12,
  height: 40,
  padding: 8,
});

const UnitLabel = styled.Text({
  width: 25,
});

const UnitInput = (props: UnitInputProps & TextInputProps): JSX.Element => {
  const {label, flex, ...inputProps} = props;
  return (
    <UnitRow flex={flex}>
      <UnitNumberInput keyboardType="numeric" {...inputProps} />
      {!!label && <UnitLabel>{label}</UnitLabel>}
    </UnitRow>
  );
};

export default UnitInput;
