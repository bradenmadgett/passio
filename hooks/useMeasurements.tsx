import {useEffect} from 'react';
import {DocumentDirectoryPath, readFile, writeFile} from 'react-native-fs';
import {Units, useUnits} from '../store/units';
import {
  feetAndInchesToMeters,
  kgToLbs,
  lbsToKg,
  metersToInches,
} from '../util/conversions';

const unitsFilePath = DocumentDirectoryPath + '/units.json';

const useMeasurements = () => {
  const {
    unit,
    pounds,
    feet,
    inches,
    meters,
    kilos,
    setUnit,
    setPounds,
    setFeet,
    setInches,
    setMeters,
    setKilos,
    setUnits,
  } = useUnits();

  // Read from json file and initialize with data
  useEffect(() => {
    readFile(unitsFilePath)
      .then(unitsFromFile => {
        if (unitsFromFile) {
          const unitsJSON: Units = JSON.parse(unitsFromFile);
          setUnits(unitsJSON);
        }
      })
      .catch((error: Error) => console.log(error.message));
  }, [setUnits]);

  const convertAndSetUnit = (newUnit: string) => {
    if (newUnit === 'imperial') {
      const totalInchesFromMeters = metersToInches(meters);
      const feetFromTotalInches = Math.trunc(totalInchesFromMeters / 12);
      const inchesFromRemainder = totalInchesFromMeters % 12;
      setFeet(feetFromTotalInches);
      setInches(inchesFromRemainder);
    } else {
      setMeters(feetAndInchesToMeters(feet, inches));
      setKilos(lbsToKg(pounds));
    }
    setUnit(newUnit);
  };

  //Ensures all values are updated and equivalent before writing to disk
  const prepareUnitDataToWrite = () => {
    if (unit === 'imperial') {
      return JSON.stringify({
        pounds: pounds,
        feet: feet,
        inches: inches,
        meters: feetAndInchesToMeters(feet, inches),
        kilos: lbsToKg(pounds),
        unit: unit,
      });
    } else {
      return JSON.stringify({
        pounds: kgToLbs(kilos),
        feet: Math.trunc(metersToInches(meters) / 12),
        inches: metersToInches(meters) % 12,
        meters: meters,
        kilos: kilos,
        unit: unit,
      });
    }
  };

  const saveToDisk = () => {
    return writeFile(unitsFilePath, prepareUnitDataToWrite());
  };

  return {
    setPounds,
    setFeet,
    setInches,
    setMeters,
    setKilos,
    convertAndSetUnit,
    pounds,
    feet,
    inches,
    meters,
    kilos,
    unit,
    saveToDisk,
  };
};

export default useMeasurements;
