/* eslint-disable prettier/prettier */
module.exports = {
  preset: 'react-native',
  transform: {
    '^.+\\.(js|ts|tsx|jsx)$': 'babel-jest',
  },
  transformIgnorePatterns: [
    'node_modules/(?!@react-native|react-native)',
  ],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
