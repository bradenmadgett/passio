import create from 'zustand';

export interface Units {
  unit: string;
  pounds: number;
  feet: number;
  inches: number;
  kilos: number;
  meters: number;
}

interface UnitsState extends Units {
  setUnit: (input: string) => void;
  setPounds: (input: number) => void;
  setFeet: (input: number) => void;
  setInches: (input: number) => void;
  setKilos: (input: number) => void;
  setMeters: (input: number) => void;
  setUnits: (input: Units) => void;
}

export const useUnits = create<UnitsState>(set => ({
  unit: 'imperial',
  pounds: 0,
  feet: 0,
  inches: 0,
  kilos: 0,
  meters: 0,
  setUnit: (newUnit: string) => set({unit: newUnit}),
  setPounds: (lbs: number) => set({pounds: lbs}),
  setFeet: (ft: number) => set({feet: ft}),
  setInches: (inch: number) => set({inches: inch}),
  setKilos: (kg: number) => set({kilos: kg}),
  setMeters: (m: number) => set({meters: m}),
  setUnits: (units: Units) => set({...units}),
}));
