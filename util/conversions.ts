export const lbsToKg = (lbs: number) => {
  return lbs * 0.453592;
};

export const kgToLbs = (kg: number) => {
  return kg / 0.453592;
};

export const feetAndInchesToMeters = (feet: number, inches: number) => {
  const totalInches = feet * 12 + inches;
  return totalInches * 0.0254;
};

export const metersToInches = (meters: number) => {
  return meters / 0.0254;
};
